<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'fpparticipa');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{]p|7B9w Y5*J44X;)-Aa.ve5B}HLre_Y [JKguiv/QX4PeZ~T^-*p%:d}~f9wuv');
define('SECURE_AUTH_KEY',  '?+t_pwdeF^Co-E|FO4ff(#>EF1,KTK<Oiu[.y!LUVIRaL{Ob,!Zq{i3ODl/OdYZ+');
define('LOGGED_IN_KEY',    '[ljKtORiIf#K0iB,4:+)D[gJB(4+2k-R{w:E-!V]Hy&7_Wk-a/POm,)H|^9+ q63');
define('NONCE_KEY',        'HG:Tmf-h~6 $om -%M*Ej$y6,$rL+hl=Jh(Lo:glO+YA+NJ<l.:BMjCXvu&!Ura|');
define('AUTH_SALT',        '[(]s8Lpp}FX-N>%XRl/qkowdR`C6lI9Q-/|_#}.sRT)EP}5X9S?PV_aB!A{oPYR{');
define('SECURE_AUTH_SALT', 'n@26JGC/)&&D4 Qavhcj9EMpvQ*Mh$gy)-ELRK0-gX?Zc@0,<AGQPoN-;*R]<Tik');
define('LOGGED_IN_SALT',   'N|gEkF-{d>`t-9L9c0#Ac??pOJ1=@l+)+et52iM~V IP=[2eJ&78*zmFuoW)^l>H');
define('NONCE_SALT',       't8B>||+C;.#>1&mC(Ezz~Ox| 8&9EFjwO6!9f9p1.V*x[tRt?!T`h%+D)>L9oxy-');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
