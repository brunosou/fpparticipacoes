<?php
/**
 * Template for displaying the footer
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
  </div><!-- #main -->

  <div id="footer" role="contentinfo">
    <div id="colophon">

<?php
  /*
   * A sidebar in the footer? Yep. You can can customize
   * your footer with four columns of widgets.
   */
  get_sidebar( 'footer' );

?>


    <div id="site-info">
        <a href="<?php //echo esc_url( home_url( '/' ) ); ?>" title="<?php //echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
          <?php //bloginfo( 'name' ); ?>
        </a>
      </div><!-- #site-info -->
    
<?php 
      $txtFooter = get_post_meta(10,'TextoFooter');
      $txtFooter2 = get_post_meta(10,'TextoFooter2');
      $contato = get_post_meta(10,'Contato');
      $endereco = get_post_meta(10,'Endereço');

      $textoFooter = (!empty($txtFooter) ? $txtFooter : ['Lorem Ipsum is simply dummy text of the printing']);
      $textoFooter2 = (!empty($txtFooter2) ? $txtFooter2 : ['Lorem Ipsum is simply dummy text of the printing']);
      $phone = (!empty($contato) ? $contato : ['123456789']);
      $address = (!empty($endereco) ? $endereco : ['Lorem Ipsum is simply dummy text of the']); 
?>

<!-- My Custom Footer Start -->
  <div class="footer_top_section">
    <div class="container">
         <div class="row"> 
               <div class="col-md-1"></div>
              <div class="col-md-10 text-center">
                <p><?php echo $textoFooter[0]; ?></p>
              </div>
              <div class="col-md-1"></div>
   
         </div>
      </div>
    </div>

    <div class="footer_midd_section">
      <div class="container">
          <div class="row">
              <div class="col-md-6">
                <h4 class="heading_footer"><?php echo $textoFooter2[0]; ?></h4>
              </div>
              <div class="col-md-2">
                <p class="no_top_bot_mar"><strong>Contato</strong></p>
                <p class="no_top_bot_mar"><?php echo $phone[0]; ?></p>
              </div>
               <div class="col-md-2">
                 <p class="no_top_bot_mar"><strong>Endereço</strong></p>
                 <p class="no_top_bot_mar"><?php echo $address[0]; ?></p>
              </div>


              <div class="col-md-2 text-center">
                <img src="http://www.fpparticipacoes.com.br/wp-content/uploads/2015/07/footer_logo.png" class="" width="55">
              </div>
   
          </div>
      </div>
    </div>

    <div class="footer_bott_section">
      <div class="container">
         <div class="row">
              <div class="col-md-6 col-xs-6 no_pad_left">
                 <p>&copy; 2015 fpparticipacoes.com.br</p>

              </div>

              <div class="col-md-6 col-xs-6 no_pad_right">
                <div class="social_icon text-right">
                          <ul>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                  
                </ul>
                       
                  </div>
                
              </div>
   
   
         </div>
      </div>
    </div>

  <!-- My Custom Footer End -->


    </div><!-- #colophon -->


  </div><!-- #footer -->

</div><!-- #wrapper -->

<?php
  /*
   * Always have wp_footer() just before the closing </body>
   * tag of your theme, or you will break many plugins, which
   * generally use this hook to reference JavaScript files.
   */

  wp_footer();
?>
</body>
</html>
