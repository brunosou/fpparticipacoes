var cMap = {

  map:'',
    /**
     * Function to init map
     */

    initialize: function(filter,cid) {

      if(!cid)
        var cid = 'map-canvas';        

      if(!filter)
        var filter = 'venda';


        var center = new google.maps.LatLng(52.4357808, 4.991315699999973);
        var mapOptions = {
            zoom: 12,
            center: center,
            mapTypeId: google.maps.MapTypeId.TERRAIN
        };

        var mapClass = $('.cmap');
        $('.cmap').each(function(index,el) {
              cMap.map = new google.maps.Map(mapClass[index], mapOptions);
              var src = $('#'+cid).attr('kml');
              var tstamp = new Date().getTime();
              var kmlUrl = src+'?c='+tstamp;
              var kmlOptions = {
                suppressInfoWindows: true,
                preserveViewport: false,
                map: cMap.map
              };

              var kmlLayer = new google.maps.KmlLayer(kmlUrl, kmlOptions);
              google.maps.event.addListener(kmlLayer, 'click', function(event) {
                var content = event.featureData.infoWindowHtml;
                var $capture = $('.capture');
                $capture.html(content);
              });  
        });
        
        // for (i = 0; i < markers1.length; i++) {
        //     cMap.addMarker(markers1[i]);
        // }
        // http://emancipa.com.br/clientes/fp/fortaleza.kml
        

        //alert('cid-> '+cid+'\nfilter-> '+filter);

        cMap.bindEvents();
    },

    /**
     * Function to add marker to map
     */

    addMarker: function(marker) {
        var category = marker[4];
        var title = marker[1];
        var pos = new google.maps.LatLng(marker[2], marker[3]);
        var content = marker[1];
        var image = 'http://www.fpparticipacoes.com.br/wp-content/themes/fpparticipacoes/images/pin.png';

        marker1 = new google.maps.Marker({
            title: title,
            position: pos,
            category: category,
            map: cMap.map,
            icon: image
        });

        gmarkers1.push(marker1);

        // Marker click listener
        google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
            return function () {
                console.log('Gmarker 1 gets pushed');
                infowindow.setContent(content);
                infowindow.open(map, marker1);
                cMap.map.panTo(this.getPosition());
                cMap.map.setZoom(15);
            }
        })(marker1, content));
    },

    /**
     * Function to filter markers by category
     */

    filterMarkers: function(category) {

        for (i = 0; i < markers1.length; i++) {
            marker = gmarkers1[i];
            // If is same category or category not picked
            if (marker.category == category || category.length === 0 || category == 0) {
                marker.setVisible(true);
            }
            // Categories don't match 
            else {
                marker.setVisible(false);
            }
        }
    },

    bindEvents: function() {
      $('.rfilter').change(function() {
        cMap.filterMarkers(this.value);
      });
    }


};
var Screen = {
  currentTab: '',

      init: function() {
        if(/home|page-template-listing/.test($('body')[0].className)) {
          this.setupCarousel();
          this.bindEvents();
        } 

        if(/page-template-listing/.test($('body')[0].className)) {
          this.selectFilter();
          //cMap.initialize();
        }

        if(/page-id-23/.test($('body')[0].className)) 
          if($('.wpl_prp_container_content').length) {
            $('.wpl_prp_container_content').addClass('container');
            $('.price_box').appendTo('.wpl_prp_show_detail_boxes_cont[itemprop=description]');
            if(!$('.headingContato').length)
              $('.wpl_prp_container_content_right .wpl_prp_right_boxes_title:first').prepend('<h3 class="headingContato">Interessado? Deixe que entramos em contato:</h3>');
          }
        
        
        this.fixMenu();
        
      },

      changeKML: function(filter,cid) {
          cMap.initialize(filter,cid);        
      },

      // first run
      selectFilter: function() {
        if( /venda/.test(location.href) )
          this.activateTab('tab_default_1','venda');        

        else if( /alugar/.test(location.href) ) 
          this.activateTab('tab_default_2','aluguel');

        else if( /galpoes/.test(location.href) )
          this.activateTab('tab_below_016','venda');

        else if( /comercial/.test(location.href) )
          this.activateTab('tab_below_017','venda');

        else if( /residencial/.test(location.href) )
          this.activateTab('tab_below_018','venda');
        
        else
          this.activateTab('tab_default_1','venda');
        
      },

      activateTab: function(tab,map) {
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');    
        this.currentTab = map;    
        // var x = (map == 'venda' ? 'map-canvas' : 'map-canvas2');
        // this.changeKML(map,x);
        //this.toggleMap(map);
      },

      toggleMap: function(map) {
        $('.mapa_venda,.mapa_aluguel').hide();
        $('.mapa_'+map).show();
        Screen.currentTab = map;
      },

      setupCarousel: function() {
        $('.carousel.slide').each(function(index,el) { 
          itens = $(el).find('.item').length;
          aclass = ' class="active"';
          $(el).append('<ol class="carousel-indicators"></ol>');
          for(var i=0;i<itens;i++) {
            if(i!==0)
              aclass = '';

            $(el).find('.carousel-indicators').append('<li data-target="#Carousel" data-slide-to="'+i+'"'+aclass+'></li>');
          }
        });
        
        $('.carousel-indicators').each(function(index,el) {
          $(el).children('li').first().addClass('active');
        });

        
      },

      fixMenu: function() {
        $('.sub-menu').each(function(index,el) { 
          $el = $(el).parent().children('a'); 
          $el.attr('style','');
          $el.attr('style','width:'+$(this).width()+'px;'); 
        });
      },

      getCurrentMap: function() {
        if(Screen.currentTab == 'venda')
          return 'map-canvas';
        else
          return 'map-canvas2';
      },

      bindEvents: function() {

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
          var target = $(e.target).attr("href") // activated tab
          if(target == '#tab_default_1') 
            Screen.toggleMap('venda');
          else if(target == '#tab_default_2')
            Screen.toggleMap('aluguel');

          cMap.initialize(Screen.currentTab,Screen.getCurrentMap());
        });


        $('.filter_text').click(function(e){
          $('.handleTabs').slideToggle("slow", function() {
            
          });
        

          e.stopPropagation();
          e.preventDefault();
        });

        
      }
};

jQuery(window).resize(function(){
  Screen.fixMenu();       
});
jQuery(document).ready(function(){
  window.$ = jQuery;
  Screen.init();       

});

