<?php
/**
 * Template Name: Contato
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container" class="one-column">
			<div id="content" role="main">

			<main class="line-breadcrumb"><div class="container"><div class="row">
				<div class="col-xs-12">
				    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p class="breadcrumb">','</p>');
                    } ?>
				</div>
		    </main>

		    <div class="contact_top_section">
			<div class="container">
			<div class="row">
			<div class="contact_form col-xs-12">
				   <h2 class="text_left">Fale conosco</h2>
			           
			<p class="cont_form_detail">Após digitar o endereço, confirme se a localização está apontada corretamente no mapa. Caso contrário, aponte no mapa a região que deseja. Para isto, basta clicar e arrastar o logo BSPAR.</p>

			<?php echo do_shortcode('[contact-form-7 id="344" title="Sem nome"]'); ?>

			</div>
			</div>
			</div>
			</div>
			

			<?php
			/*
			 * Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>

			</div><!-- #content -->
		</div><!-- #container -->

<?php get_footer(); ?>
