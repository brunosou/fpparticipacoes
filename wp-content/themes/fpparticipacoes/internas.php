<?php
/**
 * Template Name: Internas
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container" class="one-column">
			<div id="content" role="main">

			<main class="line-breadcrumb">
			<div class="container"><div class="row">
				<div class="col-xs-12">
				    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p class="breadcrumb">','</p>');
                    } ?>
				</div>
				</div></div>
		    </main>
			

			<?php
			/*
			 * Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>

			</div><!-- #content -->
		</div><!-- #container -->

<?php get_footer(); ?>
