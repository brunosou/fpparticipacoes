<?php

/**

 * Header template for our theme

 *

 * Displays all of the <head> section and everything up till <div id="main">.

 *

 * @package WordPress

 * @subpackage Twenty_Ten

 * @since Twenty Ten 1.0

 */

?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

<meta charset="<?php bloginfo( 'charset' ); ?>" />
 <meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php

	/*

	 * Print the <title> tag based on what is being viewed.

	 */

	global $page, $paged;



	wp_title( '|', true, 'right' );



	// Add the blog name.

	bloginfo( 'name' );



	// Add the blog description for the home/front page.

	$site_description = get_bloginfo( 'description', 'display' );

	if ( $site_description && ( is_home() || is_front_page() ) )

		echo " | $site_description";



	// Add a page number if necessary:

	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )

		echo esc_html( ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) ) );



	?></title>

<link rel="profile" href="http://gmpg.org/xfn/11" />

<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php

	/*

	 * We add some JavaScript to pages with the comment form

	 * to support sites with threaded comments (when in use).

	 */

	if ( is_singular() && get_option( 'thread_comments' ) )

		wp_enqueue_script( 'comment-reply' );



	/*

	 * Always have wp_head() just before the closing </head>

	 * tag of your theme, or you will break many plugins, which

	 * generally use this hook to add elements to <head> such

	 * as styles, scripts, and meta tags.

	 */

	wp_head();

?>



<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css">

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>

<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,100,200,500,600,700,800' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>


</head>



<body <?php body_class(); ?>>

<main class="top_header">

    <div class="container">

       <div class="row">

       <div class="col-md-8"></div>

           <div class="col-md-4 text-right">

              <!--  <div class="top_social_media">

                   <ul>

                      <li><a href="#">PORTAL CLIENTE <i class="fa fa-angle-right"></i></a></li>

                      <li><a href="#">ACESSO CORRETOR <i class="fa fa-angle-right"></i></a></li>

                   </ul>

                </div> 

                -->

                        

           </div>

       </div>

    </div>

</main>



<div class="header_section">

<nav id="myNavbar" class="navbar navbar-default navbar-inverse" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->

            <div class="container">

            <div class="col-md-3">

                <div class="navbar-header">

                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">

                        <span class="sr-only">Toggle navigation</span>

                        <span class="icon-bar"></span>

                        <span class="icon-bar"></span>

                        <span class="icon-bar"></span>

                    </button>



                    <a class="navbar-brand" href="http://www.fpparticipacoes.com.br"><img src="<?php echo get_bloginfo('template_url').'/images/logofp2.png'; ?>"></a>

                </div>

            </div>

            <div class="col-md-9">

                <!-- Collect the nav links, forms, and other content for toggling -->

                <div class="collapse navbar-collapse pull-right" id="navbarCollapse">

                    <?php wp_nav_menu(
                          array(
                            'menu' => 'Primary Menu',// name of menu
                            'menu_class' => 'nav navbar-nav dropdown'//ul class name
                          )
                        );


                        ?>

                </div>

            </div>

        </nav>

</div>

</nav>

</main>

