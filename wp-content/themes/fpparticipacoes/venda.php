<?php
/**
 * Template Name: Venda seu Terreno
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container" class="one-column">
			<div id="content" role="main">

			<main class="line-breadcrumb"><div class="container"><div class="row">
				<div class="col-xs-12">
				    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p class="breadcrumb">','</p>');
                    } ?>
				</div>
		    </main>

		    <div class="sell_land_page">
				<div class="contact_top_section">
				<div class="container">
					<div class="row">

						<div class="col-xs-12 contact_form">
						<h2 class="text_left"><?php the_title(); ?></h2>
						<strong>Preencha os dados</strong>
							
							<?php echo do_shortcode('[contact-form-7 id="348" title="Venda seu Terreno"]'); ?>
						
						
						
							<div class="map_shor_code">
							<p>Após digitar o endereço, confirme se a localização está apontada corretamente no mapa. Caso contrário, aponte no mapa a região que deseja.</p>
								<?php echo do_shortcode('[find_my_google_map]'); ?>
							</div>
						
						</div>
					</div>
				</div>
			</div>
			

			<?php
			/*
			 * Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>

			</div><!-- #content -->
		</div><!-- #container -->

<?php get_footer(); ?>
