<?php
/**
 * Template Name: Mapa
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<?php 
	if( isset($_GET['shortcode']) ) { 
		echo do_shortcode(stripcslashes($_GET['shortcode'])); 
	} else {
		echo do_shortcode('[WPL]'); 
	}

?>

<?php get_footer(); ?>
