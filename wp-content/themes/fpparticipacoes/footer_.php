<?php
/**
 * Template for displaying the footer
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
	<main class="footer_top_section">
    <div class="container">
       <div class="row"> 
             <div class="col-md-1"></div>
            <div class="col-md-10 text-center">
            	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has xt of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley</p>
            </div>
            <div class="col-md-1"></div>
 
       </div>
    </div>
</main>
<main class="footer_midd_section">
    <div class="container">
       <div class="row">
            <div class="col-md-6">
            	<h4>Lorem Ipsum is simply dummy text of the printing</h4>
            </div>
            <div class="col-md-2">
              <p class="no_top_bot_mar"><strong>Contact</strong></p>
              <p class="no_top_bot_mar">123456789</p>
            </div>
             <div class="col-md-2">
               <p class="no_top_bot_mar"><strong>Address</strong></p>
               <p class="no_top_bot_mar">Lorem Ipsum is simply text of the</p>
            </div>


            <div class="col-md-2 text-center">
            	<img src="http://www.fpparticipacoes.com.br/wp-content/uploads/2015/07/footer_logo.png" class="" width="55">
            </div>
 
       </div>
    </div>
</main>
<main class="footer_bott_section">
    <div class="container">
       <div class="row">
            <div class="col-md-6 no_pad_left">
            	 <p>&copy; 2015 Lorem Ipsum </p>

            </div>

            <div class="col-md-6 no_pad_right">
            	<div class="social_icon text-right">
                        <ul>
					        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
					        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
					        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
					        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
					      
					    </ul>
                     
                </div>
            	
            </div>
 
 
       </div>
    </div>
</main>
</body>
</html>
