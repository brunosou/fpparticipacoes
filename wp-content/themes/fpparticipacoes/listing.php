<?php
/**
 * Template Name: Listing
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container" class="one-column">
			<div id="content" role="main">

			<main class="line-breadcrumb">
        <div class="container"><div class="row">
				<div class="col-xs-12">
				    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p class="breadcrumb">','</p>');
                    } ?>
				</div>
        </div></div>
		    </main>

<?php

function my_custom_listing()
{
   global $wpdb;
  $select_all_property_type = "SELECT DISTINCT wp_wpl_properties.property_type, wp_wpl_property_types.name FROM wp_wpl_properties
  inner join wp_wpl_property_types
  ON wp_wpl_property_types.ID = wp_wpl_properties.property_type
  where wp_wpl_properties.listing != ''
  AND wp_wpl_properties.property_type != ''";
  $all_property_type_array = $wpdb->get_results($select_all_property_type, ARRAY_A);
  $no_of_property_type = count($all_property_type_array);

  $select_sell_property_list = "SELECT *, ID, mls_id,listing, property_type, price, meta_keywords,alias,
field_312, field_308 ,
(SELECT item_name FROM wp_wpl_items WHERE wp_wpl_items.parent_id = wp_wpl_properties.ID limit 1) as image_name,
(SELECT name FROM wp_wpl_listing_types WHERE wp_wpl_listing_types.ID = wp_wpl_properties.listing limit 1) as listing_type_name,
(SELECT name FROM wp_wpl_property_types WHERE wp_wpl_property_types.ID = wp_wpl_properties.property_type limit 1) as property_type_name,
(SELECT parent FROM wp_wpl_listing_types WHERE wp_wpl_listing_types.ID = wp_wpl_properties.listing limit 1) as listing_parent
FROM wp_wpl_properties 
where wp_wpl_properties.listing != ''
AND wp_wpl_properties.property_type != ''";
$all_property_list_array = $wpdb->get_results($select_sell_property_list, ARRAY_A);



?>
<div class="listing_sort_section">
    
      
      <div class="col-md-12 listing_category">
        <div class="tabbable-panel">
          <div class="tabbable-line">
            
            
              <ul class="nav nav-tabs top_listing_cat">
                   <div class="activity_select" id="list_select"></div>
                   <a href="#" class="filter_text text-center"><span><i class="fa fa-bars"></i></span><span><h4>Opções de Filtro</h4></span></a>
                 </ul>
            

            <div class="tab-content">
<?php
  $cnt = 0;
  while($cnt < 2)
  {
    if($cnt == 0)
    {
      ?>
              <div class="tab-pane active" id="tab_default_1">
                  <div class="tabbable-panel below_listing_cate">
                    <div class="tabbable-line tabs-below">

                      <div class="handleTabs">
                        <div class="container">
                          <ul class="nav nav-tabs left_category" id="tabsLeft">
                            <li class="active">
                              <a href="#tab_default_1" data-toggle="tab"><span><i class="fa fa-square-o"></i></span>Venda</a>
                            </li>
                            <li> 
                             
                              <a href="#tab_default_2" data-toggle="tab"><span><i class="fa fa-square-o"></i></span>Aluguel</a></li>

                         </ul>

                          <ul class="nav nav-tabs right_category below_cate_list" id="tabsRight">
                            <?php
                            $tab_count = 0;
                            while($tab_count < count($all_property_type_array))
                            {
                              ?>
                                <li class="<?php if($tab_count == 0) { echo 'active'; } ?>"><a href="#tab_below_<?php echo $cnt.$all_property_type_array[$tab_count]['property_type'];  ?>" data-toggle="tab"><span><i class="fa fa-square-o"></i></span><?php echo $all_property_type_array[$tab_count]['name']; ?></a></li>
                              <?php
                              $tab_count++;
                            }
                            ?>
                          </ul>
                        </div>
                      </div>
                       
                    <div class="tab-content">

      <?php
    }
    else if($cnt == 1)
    {
      ?>
              <div class="tab-pane" id="tab_default_2">
                  <div class="tabbable-panel below_listing_cate">
                    
                    <div class="tabbable-line tabs-below">
                    <div class="handleTabs">
                      <div class="container">
                        <ul class="nav nav-tabs left_category" id="tabsLeft">
                        <li>
                          <a href="#tab_default_1" data-toggle="tab"><span><i class="fa fa-square-o"></i></span>Venda</a>
                        </li>
                        <li class="active">
                          <a href="#tab_default_2" data-toggle="tab"><span><i class="fa fa-square-o"></i></span>Aluguel</a>
                        </li>

                      </ul>

                      <ul class="nav nav-tabs right_category below_cate_list" id="tabsRight">
                        <?php
                        $tab_count = 0;
                        while($tab_count < count($all_property_type_array))
                        {
                          ?>
                            <li class="<?php if($tab_count == 0) { echo 'active'; } ?>"><a href="#tab_below_<?php echo $cnt.$all_property_type_array[$tab_count]['property_type'];  ?>" data-toggle="tab"><span><i class="fa fa-square-o"></i></span><?php echo $all_property_type_array[$tab_count]['name']; ?></a></li>
                          <?php
                          $tab_count++;
                        }
                        ?>
                      </ul>
                  </div>
                 </div>
                <div class="tab-content">

                
                  
                      
      <?php
    }
    $property_count = 0;
          
    while($property_count < count($all_property_type_array))
    {
      ?>
                      <div class="tab-pane <?php if($property_count == 0) { echo "active"; } ?>" id="tab_below_<?php echo $cnt.$all_property_type_array[$property_count]['property_type']; ?>">
                        <div class="container">
                              <div class="col-md-6">
                                <div class="carousel fixMap">
                                   <?php /*<iframe src="/index2.php/mapa?shortcode=<?php echo urlencode(get_the_content()); ?>" ALLOWTRANSPARENCY="true" scrolling="no"></iframe> */ ?>
                                  



                                  <?php
                                  foreach(get_post_meta(get_the_ID(),'kml_listing')[0] as $val){
                                    if($val['categoria'] == 'venda')
                                      $kmlVenda = $val['kml'];
                                    else
                                      $kmlAluguel = $val['kml'];
                                  }
                                  $kml = get_post_meta(get_the_ID(),'kml_listing')[0][0]['kml'];
                                  echo '<div id="'.($cnt > 0 ? 'map-canvas2' : 'map-canvas').'" class="cmap" kml="'.($cnt > 0 ? $kmlAluguel : $kmlVenda).'"></div>';

                                  ?>
                                    <?php
                                      get_template_part('mapa','listing');
                                    ?>
                                </div>
                              </div>
                              <div id="Carousel" class="carousel slide col-md-6">
                                  <div class="carousel-inner">

      <?php
      $count = 0;
      $div_count_rent = 3;
      $div_count_sell = 3;



      $select_count_total_sell_entry = "SELECT wp_wpl_properties.property_type FROM wp_wpl_properties
      inner join wp_wpl_listing_types
      ON wp_wpl_listing_types.ID = wp_wpl_properties.listing
      AND wp_wpl_listing_types.parent = '1'
      where wp_wpl_properties.property_type = '".$all_property_type_array[$property_count]['property_type']."'";
      $total_sell_entry_arry = $wpdb->get_results($select_count_total_sell_entry, ARRAY_A);
      $total_sell_entry = count($total_sell_entry_arry);

      $select_count_total_rent_entry = "SELECT wp_wpl_properties.property_type FROM wp_wpl_properties
      inner join wp_wpl_listing_types
      ON wp_wpl_listing_types.ID = wp_wpl_properties.listing
      AND wp_wpl_listing_types.parent = '2'
      where wp_wpl_properties.property_type = '".$all_property_type_array[$property_count]['property_type']."'";
      $total_rent_entry_arry = $wpdb->get_results($select_count_total_rent_entry, ARRAY_A);
      $total_rent_entry = count($total_rent_entry_arry);


      

      while($count < count($all_property_list_array))
      {
        
        if(($all_property_list_array[$count]['listing_parent'] == '1') && $cnt == 0)
        {
          if($all_property_list_array[$count]['property_type'] == $all_property_type_array[$property_count]['property_type'])
          {
            if($div_count_sell %3 == 0)
            {
              $item = "";               
              if ($div_count_sell == 3){$item = "item active";} else {$item = "item";}
              ?>
              <div class="<?php echo $item ;?>">
              <div class="rows"> 
              <?php
            }

                    /*  Copy This  -------------------------------------------------------- FROM  */ 
                    $list_path  = '../../empreendimento';
                    $alias_link =  $list_path."/".$all_property_list_array[$count]['ID']."-".$all_property_list_array[$count]['alias']."/";
                    /*  Copy This  -------------------------------------------------------- END  */ 
                      
                     $ext = pathinfo($all_property_list_array[$count]['image_name'], PATHINFO_EXTENSION);
                     $newFileName = substr($all_property_list_array[$count]['image_name'], 0 , (strrpos($all_property_list_array[$count]['image_name'], ".")));
                     $type_name = $all_property_list_array[$count]['listing_type_name'];
                     $ptype = $all_property_list_array[$count]['property_type_name'];
                     

                    $pisox = $all_property_list_array[$count]['f_3004'];
                    $docas = $all_property_list_array[$count]['f_3005'];
                    $plata = $all_property_list_array[$count]['f_3006'];
                    $guarita = $all_property_list_array[$count]['f_3007'];

                    $quartos = ($all_property_list_array[$count]['field_3018'] ? $all_property_list_array[$count]['field_3018'] : 0);
                    $vagas = ($all_property_list_array[$count]['field_3019'] ? $all_property_list_array[$count]['field_3019'] : 0);
                    //$kml = ($all_property_list_array[$count]['field_3031'] ? $all_property_list_array[$count]['field_3031'] : 'http://kml-samples.googlecode.com/svn/trunk/kml/time/time-stamp-point.kml');
                    
                    
                    if($ptype == 'Residencial') {
                      $area = $all_property_list_array[$count]['field_3016'];
                    } else {
                      $area = ($all_property_list_array[$count]['living_area'] ? $all_property_list_array[$count]['living_area'] : 0);
                    }

                    $label_quartos = ($quartos == 1 ? 'quarto' : 'quartos');
                    $label_vagas = ($vagas == 1 ? 'vaga' : 'vagas');

                    $getdesc = $all_property_list_array[$count]['field_308'];
                    $desc = (!empty($getdesc) ? $getdesc : substr($all_property_list_array[$count]['meta_keywords'], 0, 45).'[...]');
                     
            ?>


                <div class="slider_img_first" id="slide_my_img">
                     <div class="apartment_heading"><h5><?php echo $type_name; ?></h5></div>
                     <div class="detail_heading_price innerShadow"><h3><span>R$</span> <?php $getPrice = $all_property_list_array[$count]['price']; echo number_format($getPrice,0,'.','.'); ?></h3></div>

                        <a href="<?php  echo $alias_link; ?>" class="thumbnail thumbnail_img1">
                          <img src="<?php echo content_url().'/uploads/WPL/'.$all_property_list_array[$count]['ID'].'/th'.$newFileName.'_290x290.'.$ext; ?>" alt="Image" style="max-width:100%;">
                        </a>
                        
                        <div class="cont_details">
                          <div class="apartment_label"><h5><?php echo $all_property_list_array[$count]['property_type_name']; ?></h5></div>
                          <div class="detail_heading1"><h3><?php echo substr($all_property_list_array[$count]['field_312'], 0, 20); ?></h3></div>
                          <h3><?php echo $desc; ?></h3>
                   
                          <ul>
                          <?php if($ptype == 'Residencial'): ?>
                            <li><?php echo $quartos; ?> <span><?php echo $label_quartos; ?></span></li>
                            <li><?php echo $vagas; ?> <span><?php echo $label_vagas; ?></span></li>
                          <?php endif; ?>
                            <li><?php echo $area; ?>m² <span>de área</span></li>
                            
                            <?php if($vagas > 0) { echo '<li class="garagem"><i class="glyphicon glyphicon-ok"></i>Garagem</li>'; } ?>
                            <?php if($pisox > 0) { echo '<li class="garagem"><i class="glyphicon glyphicon-ok"></i>Piso com capacidade de X</li>'; } ?>
                            <?php if($docas > 0) { echo '<li class="garagem"><i class="glyphicon glyphicon-ok"></i>Docas</li>'; } ?>
                            <?php if($plata > 0) { echo '<li class="garagem"><i class="glyphicon glyphicon-ok"></i>Plataformas</li>'; } ?>
                            <?php if($guarita > 0) { echo '<li class="garagem"><i class="glyphicon glyphicon-ok"></i>Guarita</li>'; } ?>
                            
                          </ul>

                          <a href="<?php  echo $alias_link; ?>" class="btn btn-danger">Detalhes<span class="glyphicon glyphicon-menu-right"></span></a>
                        </div>
                </div>
            <?php
            
            if((((($div_count_sell+1)%3) == 0)&&($div_count_sell > 3)) || (($div_count_sell - 2) == $total_sell_entry))
            { 
              ?> 
                </div><!--/row-->
                </div><!--/item-->
              <?php

            }
            //echo $all_property_list_array[$count]['property_type_name'];
            $div_count_sell++;
          }
        }
        if (($all_property_list_array[$count]['listing_parent'] == '2') && $cnt == 1)
        {
          if($all_property_list_array[$count]['property_type'] == $all_property_type_array[$property_count]['property_type'])
          {
            if($div_count_rent %3 == 0)
            {
              $item = "";               
              if ($div_count_rent == 3){$item = "item active";} else {$item = "item";}
              //echo $item;
              ?>
              <div class="<?php echo $item ;?>">
              <div class="rows" > 
              <?php
            }

            $type_name = $all_property_list_array[$count]['listing_type_name'];

            /*  Copy This  -------------------------------------------------------- FROM  */ 
                    $list_path2  = '../../empreendimento';
                    $alias_link2 =  $list_path2."/".$all_property_list_array[$count]['ID']."-".$all_property_list_array[$count]['alias']."/";
                    /*  Copy This  -------------------------------------------------------- END  */ 
            ?>
                
                <div class="slider_img_first" id="slide_my_img">
                     
                     <div class="apartment_heading"><h5><?php echo $type_name; ?></h5></div>
                     <div class="detail_heading_price innerShadow"><h3><span>R$</span> <?php $getPrice = $all_property_list_array[$count]['price']; echo number_format($getPrice,0,'.','.'); ?></h3></div>

                        <a href="<?php  echo $alias_link2; ?>" class="thumbnail thumbnail_img1">
                          <img src="<?php echo content_url().'/uploads/WPL/'.$all_property_list_array[$count]['ID'].'/th'.$newFileName.'_290x290.'.$ext; ?>" alt="Image" style="max-width:100%;">
                        </a>
                        
                        <div class="cont_details">
                          <div class="apartment_label"><h5><?php echo $all_property_list_array[$count]['property_type_name']; ?></h5></div>
                          <div class="detail_heading1"><h3><?php echo substr($all_property_list_array[$count]['field_312'], 0, 20); ?></h3></div>
                          <h3><?php echo substr($all_property_list_array[$count]['meta_keywords'], 0, 35).'[...]'; ?></h3>
                   
                          <ul>
                          <?php if($ptype == 'Residencial'): ?>
                            <li><?php echo $quartos; ?> <span><?php echo $label_quartos; ?></span></li>
                            <li><?php echo $vagas; ?> <span><?php echo $label_vagas; ?></span></li>
                          <?php endif; ?>
                            <li><?php echo $area; ?>m² <span>de área</span></li>
                            
                            <?php if($vagas > 0) { echo '<li class="garagem"><i class="glyphicon glyphicon-ok"></i>Garagem</li>'; } ?>
                            <?php if($pisox > 0) { echo '<li class="garagem"><i class="glyphicon glyphicon-ok"></i>Piso com capacidade de X</li>'; } ?>
                            <?php if($docas > 0) { echo '<li class="garagem"><i class="glyphicon glyphicon-ok"></i>Docas</li>'; } ?>
                            <?php if($plata > 0) { echo '<li class="garagem"><i class="glyphicon glyphicon-ok"></i>Plataformas</li>'; } ?>
                            <?php if($guarita > 0) { echo '<li class="garagem"><i class="glyphicon glyphicon-ok"></i>Guarita</li>'; } ?>
                            
                          </ul>

                          <a href="<?php  echo $alias_link2; ?>" class="btn btn-danger">Detalhes<span class="glyphicon glyphicon-menu-right"></span></a>
                        </div>
                </div>
            <?php

            

            if((((($div_count_rent+1)%3) == 0)&&($div_count_rent > 3)) || (($div_count_rent - 2) == $total_rent_entry))
            { 
              ?> 
                </div><!--/row-->
                </div><!--/item-->
              <?php
            }
            $div_count_rent++;
            //echo $all_property_list_array[$count]['property_type_name'];
          }
        }
        $count++;
        $div_count++;
      }
      ?>
                                  </div><!--.carousel-inner-->
                                  


                              </div><!--.Carousel-->
                        </div>
                      </div><!--tab_below_1 End-->

      <?php

      $property_count++;
    }
    if($cnt == 0)
    {
      ?>
                    </div><!--tab-content End-->
                    
                  </div>
                </div><!--tabbable-panel End-->
                </div>

      <?php
    }
    else if($cnt == 1)
    {
      ?>
                    </div><!--tab-content End-->
                    
                  </div>
                </div><!--tabbable-panel End-->
                </div>

      <?php
    }

    $cnt++;
  }
  ?>
            </div>
          </div>
        </div><!--top tabbable-panel End--> 
      </div>
    </div>
    </div>
</div>

  <?php
}

my_custom_listing();
?>



			</div><!-- #content -->
		
<?php get_footer(); ?>