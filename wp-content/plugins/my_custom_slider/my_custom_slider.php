<?php
/**
* Plugin Name: my custom slider
* Plugin URI: 
* Description: This plugin adds Status bar as Per Stock, Time Countdown as per End Date And Can Change Image Color to our single posts.
* Version: 1.0.0
* Author: Rj.
* Author URI: http://www.itmates.in
* License: WEB2
*/

function find_my_google_map()
{
  ?>
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:500,400italic,100,300,500italic,300italic,400,200" type="text/css" />
  <script src="http://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">    
    jQuery(window).on('load',  function() {
            new JCaption('img.caption');
            $('.hasTooltip').tooltip({"html": true,"container": "body"});
    });
  </script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
  <style type="text/stylesheet">
    @-webkit-viewport   { width: device-width; }
    @-moz-viewport      { width: device-width; }
    @-ms-viewport       { width: device-width; }
    @-o-viewport        { width: device-width; }
    @viewport           { width: device-width; }
  </style>
  <script type="text/javascript">
    //<![CDATA[
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
      var msViewportStyle = document.createElement("style");
      msViewportStyle.appendChild(
        document.createTextNode("@-ms-viewport{width:auto!important}")
      );
      document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
    }
    //]]>
  </script>
<meta name="HandheldFriendly" content="true"/>
<meta name="apple-mobile-web-app-capable" content="YES"/>
<meta name="google-site-verification" content="iD_X10CcCcgJfT4IZJ8Un-orddTOeuISR8CZ9R8VXFw" />
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-17663461-1', 'auto');
  ga('send', 'pageview');

</script>



      <link rel="styleSheet" href="http://mondeca.com/mdc_css/weather.css" TYPE="text/css" />
      <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
      <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.simpleWeather/3.0.2/jquery.simpleWeather.min.js"></script>
      <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyBVTKLztwVOGDuo1qGsjHzdY7wXRcKbAVI"> </script>
      </script>
      <script>
  function load() {

      if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById("map"));
        map.addControl(new GSmallMapControl());
        map.addControl(new GMapTypeControl());
        var center = new GLatLng(48.87146, 2.35500);
        map.setCenter(center, 15);
        geocoder = new GClientGeocoder();
        var marker = new GMarker(center, {draggable: true});  
        map.addOverlay(marker);
        document.getElementById("lat").value = center.lat().toFixed(5);
        document.getElementById("lng").value = center.lng().toFixed(5);

        
        
             
        
    GEvent.addListener(marker, "dragend", function() {
        ga('send', 'event', 'map', 'drag/move', 'map');
       var point = marker.getPoint();
        map.panTo(point);
       document.getElementById("lat").value = point.lat().toFixed(5);
       document.getElementById("lng").value = point.lng().toFixed(5);

        });
       

   GEvent.addListener(map, "moveend", function() {
       ga('send', 'event', 'map', 'drag/move', 'map');
      map.clearOverlays();
    var center = map.getCenter();
      var marker = new GMarker(center, {draggable: true});
      map.addOverlay(marker);
      document.getElementById("lat").value = center.lat().toFixed(5);
     document.getElementById("lng").value = center.lng().toFixed(5);


   GEvent.addListener(marker, "dragend", function() {
       
       ga('send', 'event', 'map', 'drag/move', 'map');
      var point =marker.getPoint();
       map.panTo(point);
      document.getElementById("lat").value = point.lat().toFixed(5);
       document.getElementById("lng").value = point.lng().toFixed(5);

        });
 
        });

      }
    }

     function showAddress(address) {
     var map = new GMap2(document.getElementById("map"));
       map.addControl(new GSmallMapControl());
       map.addControl(new GMapTypeControl());
       if (geocoder) {
        geocoder.getLatLng(
          address,
          function(point) {
            if (!point) {
              alert(address + " not found");
            } else {
      document.getElementById("lat").value = point.lat().toFixed(5);
     document.getElementById("lng").value = point.lng().toFixed(5);
     map.clearOverlays()
      map.setCenter(point, 14);
   var marker = new GMarker(point, {draggable: true});  
     map.addOverlay(marker);

    GEvent.addListener(marker, "dragend", function() {
      var pt = marker.getPoint();
       map.panTo(pt);
      document.getElementById("lat").value = pt.lat().toFixed(5);
       document.getElementById("lng").value = pt.lng().toFixed(5);
        });


   GEvent.addListener(map, "moveend", function() {
      map.clearOverlays();
    var center = map.getCenter();
      var marker = new GMarker(center, {draggable: true});
      map.addOverlay(marker);
      document.getElementById("lat").value = center.lat().toFixed(5);
     document.getElementById("lng").value = center.lng().toFixed(5);

   GEvent.addListener(marker, "dragend", function() {
     var pt = marker.getPoint();
      map.panTo(pt);
    document.getElementById("lat").value = pt.lat().toFixed(5);
     document.getElementById("lng").value = pt.lng().toFixed(5);
        });
 
              
        });

            }
          }
        );
      } 
      document.getElementById("endereo").value = address;
    }
  if(window.attachEvent) {
    window.attachEvent('onload', load);
} else {
    if(window.onload) {
        var curronload = window.onload;
        var newonload = function() {
            curronload();
            load();
        };
        window.onload = newonload;
    } else {
        window.onload = load;
    }
}
</script>

 <form class="form-inline map_search_loc" id="address" style="width:97%;" action="#" onsubmit="showAddress(this.address.value);
                                          ga('send', 'event', 'form', 'submit', 'address');                                                            
                                      return false"> 

         <div class="col-md-8">                                         
            <input type="text" size="" name="address" id="inputaddress" class="form-control" value="rua vicente linhares, 1300 brazil" style="width:100%;  margin: 38px 0; border-radius: 0;" /> 
         </div> 
        <div class="col-md-4">
          <input type="submit" value="Buscar" class="btn btn_search btn-block btn-mondeca" />
        </div>
      </form>

    <div align="center" id="map" style="min-height:300px;"></div>
  
    
   
    </div>
  

<script type="text/javascript">
var formaddress = document.getElementById("address");

formaddress.addEventListener("focusin", myFocusFunction);
formaddress.addEventListener("focusout", myBlurFunction);

function myFocusFunction() {
    document.getElementById("weather").innerHTML = ""; 

                 
}

function myBlurFunction() {
    document.getElementById("weather").innerHTML = ""; 
}

</script>


              

  <?php
}
add_shortcode( 'find_my_google_map', 'find_my_google_map' );





function my_custom_feture_listing()
{
  global $wpdb;
  $select_all_property_type = "SELECT DISTINCT wp_wpl_properties.property_type, wp_wpl_property_types.name FROM wp_wpl_properties
  inner join wp_wpl_property_types
  ON wp_wpl_property_types.ID = wp_wpl_properties.property_type
  where wp_wpl_properties.listing != ''
  AND wp_wpl_properties.property_type != ''";
  $all_property_type_array = $wpdb->get_results($select_all_property_type, ARRAY_A);
  $no_of_property_type = count($all_property_type_array);

  $select_sell_property_list = "SELECT ID, mls_id,listing, property_type, price, meta_keywords,alias,
field_312, field_308 ,
(SELECT item_name FROM wp_wpl_items WHERE wp_wpl_items.parent_id = wp_wpl_properties.ID limit 1) as image_name,
(SELECT name FROM wp_wpl_listing_types WHERE wp_wpl_listing_types.ID = wp_wpl_properties.listing limit 1) as listing_type_name,
(SELECT name FROM wp_wpl_property_types WHERE wp_wpl_property_types.ID = wp_wpl_properties.property_type limit 1) as property_type_name,
(SELECT parent FROM wp_wpl_listing_types WHERE wp_wpl_listing_types.ID = wp_wpl_properties.listing limit 1) as listing_parent
FROM wp_wpl_properties 
where wp_wpl_properties.listing != ''
AND wp_wpl_properties.property_type != ''";
$all_property_list_array = $wpdb->get_results($select_sell_property_list, ARRAY_A);

?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<?php 
  $geturl = wp_get_attachment_image_src( get_post_thumbnail_id( 10 ),'full' );
  $url = (!empty( $geturl ) ? $geturl : ['http://www.fpparticipacoes.com.br/wp-content/uploads/2015/06/m-banner.jpg']);


?>
<div class="hero_section" style="background-image:url(<?php echo $url[0]; ?>);">
 <div class="feature_listing">
            <div class="col-sm-9"></div>
            
            <div class="colListing">
                
                <?php 
                    $list_path  = 'empreendimento';
                    // setup destaques
                    $destaques = get_post_meta(10,'destaqueHome')[0];
                    $total = count($destaques);
                    
                    for($i = 0;$i < $total;$i++){                    

                      $metatitle = $destaques[$i]['titulo'];
                      $metadesc = $destaques[$i]['descricao'];
                      $alias_link = $destaques[$i]['link'];
                  ?>

                  <div class="atendimento-online-lateral" id="expand_menu<?php echo $i; ?>" >
                       <button class="button-atendimento-online" type="button">
                           <i title="Clique para fechar" class="fa fa-chevron-right right" id="right"></i>
                       </button>

                       <a href="<?php echo $alias_link; ?>" title="<?php echo $metatitle; ?>" class="click pop-up-venda-online">
                           <span class="title"><?php echo $metatitle; ?></span>
                           <span class="horario"><?php echo $metadesc; ?></span>
                       </a>
                   </div>
                <?php } ?>

              </div>
              </div>
   <div class="container">
       <div class="row text-center">
      
       </div>
   </div>
</div>


<?php
}

add_shortcode( 'my_custom_feture_listing', 'my_custom_feture_listing' );
add_action( 'my_custom_feture_listing', 'my_custom_feture_listing' );

function my_custom_slider()
{
  global $wpdb;
  $select_all_property_type = "SELECT DISTINCT wp_wpl_properties.property_type, wp_wpl_property_types.name FROM wp_wpl_properties
  inner join wp_wpl_property_types
  ON wp_wpl_property_types.ID = wp_wpl_properties.property_type
  where wp_wpl_properties.listing != ''
  AND wp_wpl_properties.property_type != ''";
  $all_property_type_array = $wpdb->get_results($select_all_property_type, ARRAY_A);
  $no_of_property_type = count($all_property_type_array);

  $select_sell_property_list = "SELECT ID, mls_id,listing, property_type, price, meta_keywords,alias,
field_312, field_308 ,
(SELECT item_name FROM wp_wpl_items WHERE wp_wpl_items.parent_id = wp_wpl_properties.ID limit 1) as image_name,
(SELECT name FROM wp_wpl_listing_types WHERE wp_wpl_listing_types.ID = wp_wpl_properties.listing limit 1) as listing_type_name,
(SELECT name FROM wp_wpl_property_types WHERE wp_wpl_property_types.ID = wp_wpl_properties.property_type limit 1) as property_type_name,
(SELECT parent FROM wp_wpl_listing_types WHERE wp_wpl_listing_types.ID = wp_wpl_properties.listing limit 1) as listing_parent
FROM wp_wpl_properties 
where wp_wpl_properties.listing != ''
AND wp_wpl_properties.property_type != ''";
$all_property_list_array = $wpdb->get_results($select_sell_property_list, ARRAY_A);
?>
<div class="listing_sort_section">
    <div class="container">
      <div class="row">
      <div class="col-md-12 listing_category">
        <div class="tabbable-panel">
          <div class="tabbable-line">
            
            
              <ul class="nav nav-tabs top_listing_cat">
                   <div class="activity_select" id="list_select"></div>
                   <a href="#" class="filter_text text-center"><span><i class="fa fa-bars"></i></span><span><h4>Opções de Filtro</h4></span></a>
                 </ul>
            

            <div class="tab-content">
<?php
  $cnt = 0;
  while($cnt < 2)
  {
    if($cnt == 0)
    {
      ?>
              <div class="tab-pane active" id="tab_default_1">
                  <div class="tabbable-panel below_listing_cate">
                    <div class="tabbable-line tabs-below">

                      <div class="handleTabs">
                        <ul class="nav nav-tabs left_category" id="tabsLeft">
                          <li class="active">
                            <a href="#tab_default_1" data-toggle="tab"><span><i class="fa fa-square-o"></i></span>Venda</a>
                          </li>
                          <li> 
                           
                            <a href="#tab_default_2" data-toggle="tab"><span><i class="fa fa-square-o"></i></span>Aluguel</a></li>

                       </ul>

                        <ul class="nav nav-tabs right_category below_cate_list" id="tabsRight">
                          <?php
                          $tab_count = 0;
                          while($tab_count < count($all_property_type_array))
                          {
                            ?>
                              <li class="<?php if($tab_count == 0) { echo 'active'; } ?>"><a href="#tab_below_<?php echo $cnt.$all_property_type_array[$tab_count]['property_type'];  ?>" data-toggle="tab"><span><i class="fa fa-square-o"></i></span><?php echo $all_property_type_array[$tab_count]['name']; ?></a></li>
                            <?php
                            $tab_count++;
                          }
                          ?>
                        </ul>
                      </div>
                       
                    <div class="tab-content">

      <?php
    }
    else if($cnt == 1)
    {
      ?>
              <div class="tab-pane" id="tab_default_2">
                  <div class="tabbable-panel below_listing_cate">
                    
                    <div class="tabbable-line tabs-below">
                    <div class="handleTabs">
                      <ul class="nav nav-tabs left_category" id="tabsLeft">
                      <li>
                        <a href="#tab_default_1" data-toggle="tab"><span><i class="fa fa-square-o"></i></span>Venda</a>
                      </li>
                      <li class="active">
                        <a href="#tab_default_2" data-toggle="tab"><span><i class="fa fa-square-o"></i></span>Aluguel</a>
                      </li>

                    </ul>

                    <ul class="nav nav-tabs right_category below_cate_list" id="tabsRight">
                      <?php
                      $tab_count = 0;
                      while($tab_count < count($all_property_type_array))
                      {
                        ?>
                          <li class="<?php if($tab_count == 0) { echo 'active'; } ?>"><a href="#tab_below_<?php echo $cnt.$all_property_type_array[$tab_count]['property_type'];  ?>" data-toggle="tab"><span><i class="fa fa-square-o"></i></span><?php echo $all_property_type_array[$tab_count]['name']; ?></a></li>
                        <?php
                        $tab_count++;
                      }
                      ?>
                    </ul>
                </div>
                <div class="tab-content">

                
                  
                      
      <?php
    }
    $property_count = 0;
    while($property_count < count($all_property_type_array))
    {
      ?>
                      <div class="tab-pane <?php if($property_count == 0) { echo "active"; } ?>" id="tab_below_<?php echo $cnt.$all_property_type_array[$property_count]['property_type']; ?>">
                        <div class="col-md-12">
                              
                              <div id="Carousel" class="carousel slide">
                                  <div class="carousel-inner">

      <?php
      $count = 0;
      $div_count_rent = 3;
      $div_count_sell = 3;

      $select_count_total_sell_entry = "SELECT wp_wpl_properties.property_type FROM wp_wpl_properties
      inner join wp_wpl_listing_types
      ON wp_wpl_listing_types.ID = wp_wpl_properties.listing
      AND wp_wpl_listing_types.parent = '1'
      where wp_wpl_properties.property_type = '".$all_property_type_array[$property_count]['property_type']."'";
      $total_sell_entry_arry = $wpdb->get_results($select_count_total_sell_entry, ARRAY_A);
      $total_sell_entry = count($total_sell_entry_arry);

      $select_count_total_rent_entry = "SELECT wp_wpl_properties.property_type FROM wp_wpl_properties
      inner join wp_wpl_listing_types
      ON wp_wpl_listing_types.ID = wp_wpl_properties.listing
      AND wp_wpl_listing_types.parent = '2'
      where wp_wpl_properties.property_type = '".$all_property_type_array[$property_count]['property_type']."'";
      $total_rent_entry_arry = $wpdb->get_results($select_count_total_rent_entry, ARRAY_A);
      $total_rent_entry = count($total_rent_entry_arry);

      while($count < count($all_property_list_array))
      {
        
        if(($all_property_list_array[$count]['listing_parent'] == '1') && $cnt == 0)
        {
          if($all_property_list_array[$count]['property_type'] == $all_property_type_array[$property_count]['property_type'])
          {
            if($div_count_sell %3 == 0)
            {
              $item = "";               
              if ($div_count_sell == 3){$item = "item active";} else {$item = "item";}
              ?>
              <div class="<?php echo $item ;?>">
              <div class="rows" > 
              <?php
            }

                    /*  Copy This  -------------------------------------------------------- FROM  */ 
                    $list_path  = 'empreendimento';
                    $alias_link =  $list_path."/".$all_property_list_array[$count]['ID']."-".$all_property_list_array[$count]['alias']."/";
                    /*  Copy This  -------------------------------------------------------- END  */ 
                      
                     $ext = pathinfo($all_property_list_array[$count]['image_name'], PATHINFO_EXTENSION);
                     $newFileName = substr($all_property_list_array[$count]['image_name'], 0 , (strrpos($all_property_list_array[$count]['image_name'], ".")));
                     $type_name = $all_property_list_array[$count]['listing_type_name'];
                     //$getdesc = $all_property_list_array[$count]['field_308'];
                     //$desc = (!empty($getdesc) ? $getdesc : substr($all_property_list_array[$count]['meta_keywords'], 0, 45).'[...]');
            ?>


                <div class="col-md-4 slider_img_first" id="slide_my_img">
                     <div class="apartment_label"><h5><?php echo $type_name; ?></h5></div>
                     <div class="apartment_heading"><h5><?php echo $all_property_list_array[$count]['property_type_name']; ?></h5></div>
                     <div class="detail_heading_price innerShadow"><h3><font style="font-size:23px; font-weight: 100; position: relative; text-shadow: -1px 0 0; top: -5px;">R$</font> <?php $getPrice = $all_property_list_array[$count]['price']; echo number_format($getPrice,0,'.','.'); ?></h3></div>

                            <a href="<?php  echo $alias_link; ?>" class="thumbnail thumbnail_img1">
                              <img src="<?php echo content_url().'/uploads/WPL/'.$all_property_list_array[$count]['ID'].'/th'.$newFileName.'_290x290.'.$ext; ?>" alt="Image" style="max-width:100%;">
                            </a>
                            
                                <div class="cont_details">
                                     <div class="detail_heading1"><h3><?php echo substr($all_property_list_array[$count]['field_312'], 0, 20); ?></h3></div>
                                     <h3><?php echo $all_property_list_array[$count]['field_308']; ?></h3>
                                     
                                </div>
                          </div>
            <?php
            
            if((((($div_count_sell+1)%3) == 0)&&($div_count_sell > 3)) || (($div_count_sell - 2) == $total_sell_entry))
            { 
              ?> 
                </div><!--/row-->
                </div><!--/item-->
              <?php

            }
            //echo $all_property_list_array[$count]['property_type_name'];
            $div_count_sell++;
          }
        }
        if (($all_property_list_array[$count]['listing_parent'] == '2') && $cnt == 1)
        {
          if($all_property_list_array[$count]['property_type'] == $all_property_type_array[$property_count]['property_type'])
          {
            if($div_count_rent %3 == 0)
            {
              $item = "";               
              if ($div_count_rent == 3){$item = "item active";} else {$item = "item";}
              //echo $item;
              ?>
              <div class="<?php echo $item ;?>">
              <div class="rows" > 
              <?php
            }

            $type_name = $all_property_list_array[$count]['listing_type_name'];

            /*  Copy This  -------------------------------------------------------- FROM  */ 
                    $list_path2  = 'empreendimento';
                    $alias_link2 =  $list_path2."/".$all_property_list_array[$count]['ID']."-".$all_property_list_array[$count]['alias']."/";
                    /*  Copy This  -------------------------------------------------------- END  */ 
            ?>
                <div class="col-md-4 slider_img_first">
                            <a href="<?php  echo $alias_link2; ?>" class="thumbnail thumbnail_img2">
                                <div class="apartment_label"><h5><?php echo $type_name; ?></h5></div>
                                <div class="apartment_heading"><h5><?php echo $all_property_list_array[$count]['property_type_name']; ?></h5></div>
                                <div class="detail_heading_price"><h3><font style="font-size:23px; font-weight: 100; position: relative; text-shadow: -1px 0 0; top: -5px;">R$</font> <?php $getPrice2 = $all_property_list_array[$count]['price']; echo number_format($getPrice2,0,'.','.'); ?></h3></div>

                              
                              <img src="<?php echo content_url().'/uploads/WPL/'.$all_property_list_array[$count]['ID'].'/thumbnail/'.$all_property_list_array[$count]['image_name']; ?>" alt="Image" style="max-width:100%;">
                            </a>
                            
                                <div class="cont_details">
                                      <div class="detail_heading1"><h3><?php echo substr($all_property_list_array[$count]['field_312'], 0, 20); ?></h3></div>
                                     <h3><?php echo $all_property_list_array[$count]['field_308']; ?></h3>
                                </div>
                          </div>
            <?php
            if((((($div_count_rent+1)%3) == 0)&&($div_count_rent > 3)) || (($div_count_rent - 2) == $total_rent_entry))
            { 
              ?> 
                </div><!--/row-->
                </div><!--/item-->
              <?php
            }
            $div_count_rent++;
            //echo $all_property_list_array[$count]['property_type_name'];
          }
        }
        $count++;
        
      }
      ?>
                                  </div><!--.carousel-inner-->
                                  <a data-slide="prev" href="#Carousel" class="left carousel-control">‹</a>
                                  <a data-slide="next" href="#Carousel" class="right carousel-control">›</a>


                              </div><!--.Carousel-->
                        </div>
                      </div><!--tab_below_1 End-->

      <?php

      $property_count++;
    }
    if($cnt == 0)
    {
      ?>
                    </div><!--tab-content End-->
                    
                  </div>
                </div><!--tabbable-panel End-->
                </div>

      <?php
    }
    else if($cnt == 1)
    {
      ?>
                    </div><!--tab-content End-->
                    
                  </div>
                </div><!--tabbable-panel End-->
                </div>

      <?php
    }

    $cnt++;
  }
  ?>
            </div>
          </div>
        </div><!--top tabbable-panel End--> 
      </div>
    </div>
    </div>
</div>

  <?php
}

add_shortcode('my_custom_slider','my_custom_slider');
?>
